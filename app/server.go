package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
)

func main() {

	flags := ParseFlags()

	ctx, _ := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)

	server := NewServer(
		ServerConfig{
			Listen: flags.Listen,
		},
		&Redis{
			NewMemoryStore(),
		},
	)

	log.Println("started jkredis")

	err := server.Run(ctx)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("k bye")
}

type Server struct {
	config ServerConfig
	redis  *Redis
}

type ServerConfig struct {
	Listen string
}

func NewServer(config ServerConfig, redis *Redis) *Server {
	return &Server{
		config,
		redis,
	}
}

func (s *Server) Run(ctx context.Context) error {

	listener, err := net.Listen("tcp", s.config.Listen)
	if err != nil {
		return err
	}

	errs := make(chan error)
	go func() {
		for {
			conn, err := listener.Accept()
			if err != nil {
				errs <- err
			}

			log.Printf("client connected: %v", conn.RemoteAddr())

			go s.handle(ctx, conn)
		}
	}()

	select {
	case err = <-errs:
	case <-ctx.Done():
	}

	listener.Close()

	return err
}

func (s *Server) handle(ctx context.Context, conn net.Conn) {

	defer conn.Close()

	lines := bufio.NewScanner(conn)

	for {

		val, err := ScanResp(lines)
		if err != nil {
			writeError(ctx, conn, err)
			break
		}

		switch val.(type) {

		case []interface{}:
			vals := val.([]interface{})
			cmd := vals[0].(string)
			args := []interface{}{}
			if len(vals) > 1 {
				args = vals[1:]
			}
			s.handleCommand(ctx, conn, cmd, args...)

		case string:
			vals := strings.Split(val.(string), " ")
			cmd := vals[0]
			args := []interface{}{}
			if len(vals) > 1 {
				for _, v := range vals[1:] {
					args = append(args, v)
				}
			}
			s.handleCommand(ctx, conn, cmd, args...)

		default:
			writeError(ctx, conn, fmt.Errorf("invalid command type"))
		}
	}

	log.Printf("client closed: %v", conn.RemoteAddr())
}

func (s *Server) handleCommand(ctx context.Context, conn net.Conn, command string, args ...interface{}) {

	log.Printf("command \"%s\" from client %v", command, conn.RemoteAddr())

	retval, err := s.redis.Command(command, args...)
	if err != nil {
		writeError(ctx, conn, fmt.Errorf("command error: %v", err))
		return
	}

	switch retval.(type) {
	case string:
		writeString(ctx, conn, retval.(string))
	case int:
		writeInt(ctx, conn, retval.(int))
	default:
		writeError(ctx, conn, fmt.Errorf("unknown resp type"))
	}
}

func writeError(ctx context.Context, conn net.Conn, e error) {

	_, err := conn.Write([]byte("-" + e.Error() + "\r\n"))
	if err != nil {
		log.Printf("client write error: %v", err)
		conn.Close()
	}
}

func writeString(ctx context.Context, conn net.Conn, str string) {

	_, err := conn.Write([]byte("+" + str + "\r\n"))
	if err != nil {
		log.Printf("client write error: %v", err)
		conn.Close()
	}
}

func writeInt(ctx context.Context, conn net.Conn, i int) {

	_, err := conn.Write([]byte(fmt.Sprintf(":%d\r\n", i)))
	if err != nil {
		log.Printf("client write error: %v", err)
		conn.Close()
	}
}
