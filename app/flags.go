package main

import "github.com/namsral/flag"

type flags struct {
	Listen string
}

func ParseFlags() *flags {

	f := &flags{}
	flag.StringVar(&f.Listen, "listen", "0.0.0.0:6379", "host:port to listen on")
	flag.Parse()

	return f
}
