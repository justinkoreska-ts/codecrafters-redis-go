package main

import (
	"context"
	"fmt"
	"testing"
)

func TestMemoryStore(t *testing.T) {

	ctx := context.Background()

	store := NewMemoryStore()

	t.Run("set get delete", func(t *testing.T) {

		key := "testkey"
		val := "testval"

		err := store.Set(ctx, key, val)
		if err != nil {
			t.Fatal(err)
		}

		v, err := store.Get(ctx, key)
		if err != nil {
			t.Fatal(err)
		}
		if v != val {
			t.Fatalf("expected val %v for key %v got %v", val, key, v)
		}

		err = store.Delete(ctx, key)
		if err != nil {
			t.Fatal(err)
		}
		v, err = store.Get(ctx, key)
		if v != "" {
			t.Fatal("expected key to be deleted")
		}
	})

	t.Run("key not exists error", func(t *testing.T) {

		_, err := store.Get(ctx, "some-random-key-name-8hfiwelsf023r")
		if err == nil {
			t.Fatal("expected error on get")
		}

		err = store.Delete(ctx, "some-random-key-name-apo25degb7wesjdg")
		if err == nil {
			t.Fatal("expected error on delete")
		}
	})

	t.Run("concurrency", func(t *testing.T) {

		f := func() {
			for n := 1; n < 1000; n++ {
				store.Set(context.Background(), fmt.Sprintf("key%d", n), fmt.Sprintf("val%d", n))
			}
		}

		for n := 0; n < 10; n++ {
			go f()
		}

		err := store.Set(context.Background(), "key0", "val0")
		if err != nil {
			t.Fatal(err)
		}
	})
}
