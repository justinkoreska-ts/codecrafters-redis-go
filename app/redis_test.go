package main

import (
	"bufio"
	"strings"
	"testing"
)

func TestRedis(t *testing.T) {

	redis := &Redis{}

	t.Run("ping", func(t *testing.T) {

		resp, err := redis.Command("PING")
		if err != nil {
			t.Fatal(err)
		}

		if resp.(string) != "PONG" {
			t.Fatal("expected string \"PONG\"")
		}
	})

	t.Run("case insensitive", func(t *testing.T) {

		_, err := redis.Command("PiNg")
		if err != nil {
			t.Fatal(err)
		}
	})
}

func TestRespParsing(t *testing.T) {

	t.Run("empty", func(t *testing.T) {

		scanner := bufio.NewScanner(strings.NewReader("\r\n"))

		_, err := ScanResp(scanner)
		if err == nil {
			t.Fatal("expected error")
		}
	})

	t.Run("array with string", func(t *testing.T) {

		scanner := bufio.NewScanner(strings.NewReader("*1\r\n+PING\r\n"))

		resp, err := ScanResp(scanner)
		if err != nil {
			t.Fatal(err)
		}

		array, ok := resp.([]interface{})
		if !ok {
			t.Fatal("expected array")
		}

		if len(array) != 1 {
			t.Fatal("expected 1 array elements")
		}

		if array[0].(string) != "PING" {
			t.Fatal("expected first array element to be \"PING\"")
		}
	})

	t.Run("array with int, error, string", func(t *testing.T) {

		scanner := bufio.NewScanner(strings.NewReader("*3\r\n:1\r\n-error\r\n+string\r\n"))

		resp, err := ScanResp(scanner)
		if err != nil {
			t.Fatal(err)
		}

		array, ok := resp.([]interface{})
		if !ok {
			t.Fatal("expected array")
		}

		if len(array) != 3 {
			t.Fatal("expected 3 array elements")
		}

		if i, ok := array[0].(int); !ok || i != 1 {
			t.Fatal("expected first array element to be int equal to 1")
		}
		if e, ok := array[1].(error); !ok || e.Error() != "error" {
			t.Fatal("expected second array element to be error equal to \"error\"")
		}
		if s, ok := array[2].(string); !ok || s != "string" {
			t.Fatal("expected first array element to be string equal to \"string\"")
		}
	})

	t.Run("bulk string", func(t *testing.T) {

		scanner := bufio.NewScanner(strings.NewReader("$4\r\nFOUR\r\n"))

		resp, err := ScanResp(scanner)
		if err != nil {
			t.Fatal(err)
		}

		bulk, ok := resp.(string)
		if !ok {
			t.Fatal("expected string")
		}
		if bulk != "FOUR" {
			t.Fatal("expected string equal to \"FOUR\"")
		}
	})

	t.Run("bulk string with newlines", func(t *testing.T) {

		scanner := bufio.NewScanner(strings.NewReader("$5\r\nFIVE\r\nextra\r\n"))

		resp, err := ScanResp(scanner)
		if err != nil {
			t.Fatal(err)
		}

		bulk, ok := resp.(string)
		if !ok {
			t.Fatal("expected string")
		}
		if bulk != "FIVE\n" {
			t.Fatal("expected string equal to \"FIVE\\n\"")
		}
	})
}
