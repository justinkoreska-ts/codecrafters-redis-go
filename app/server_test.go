package main

import (
	"context"
	"testing"
	"time"
)

func TestServer(t *testing.T) {

	server := NewServer(
		ServerConfig{
			":9999",
		},
		&Redis{},
	)

	t.Run("Run() context", func(t *testing.T) {

		long, longCancel := context.WithTimeout(context.Background(), time.Second)
		defer longCancel()

		short, shortCancel := context.WithTimeout(context.Background(), time.Second/10)
		defer shortCancel()

		done := make(chan bool)
		go func() {
			server.Run(short)
			done <- true
		}()

		select {
		case <-done:
		case <-long.Done():
			t.Fatal("expected Run() to stop")
		}
	})
}
