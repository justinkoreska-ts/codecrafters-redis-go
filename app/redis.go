package main

import (
	"bufio"
	"context"
	"fmt"
	"strconv"
	"strings"
)

type Redis struct {
	store Store
}

func (r *Redis) Command(command string, args ...interface{}) (interface{}, error) {

	switch strings.ToUpper(command) {
	case "PING":
		return r.echo(args), nil
	case "ECHO":
		return r.echo(args), nil
	case "GET":
		return r.get(args)
	case "SET":
		return "OK", r.set(args)
	case "DEL":
		return r.del(args)
	default:
		return nil, fmt.Errorf("\"%v\" not implemented", command)
	}
}

func (r *Redis) echo(args []interface{}) string {

	if len(args) > 0 {
		s := ""
		for _, a := range args {
			s += a.(string)
		}
		return s
	}

	return "PONG"
}

func (r *Redis) get(args []interface{}) (string, error) {

	if len(args) < 1 {
		return "", fmt.Errorf("expected key")
	}

	key, ok := args[0].(string)
	if !ok {
		return "", fmt.Errorf("expected string key")
	}

	return r.store.Get(context.Background(), key)
}

func (r *Redis) set(args []interface{}) error {

	if len(args) < 2 {
		return fmt.Errorf("expected key and value")
	}

	key, ok := args[0].(string)
	if !ok {
		return fmt.Errorf("expected string key")
	}
	val, ok := args[1].(string)
	if !ok {
		return fmt.Errorf("expected string value")
	}

	return r.store.Set(context.Background(), key, val)
}

func (r *Redis) del(args []interface{}) (int, error) {

	n := 0

	for _, v := range args {

		key, ok := v.(string)
		if !ok {
			return n, fmt.Errorf("expected string key")
		}

		err := r.store.Delete(context.Background(), key)
		if err == nil {
			n++
		}
	}

	return n, nil
}

const (
	RespString = "+"
	RespError  = "-"
	RespInt    = ":"
	RespBulk   = "$"
	RespArray  = "*"
)

func ScanResp(scanner *bufio.Scanner) (interface{}, error) {

	line := ""
	if scanner.Scan() {
		line = scanner.Text()
	}
	if len(line) < 2 {
		return nil, fmt.Errorf("malformed resp")
	}

	kind := string(line[0])
	val := line[1:]

	switch kind {

	case RespString:
		return val, nil

	case RespError:
		return fmt.Errorf(val), nil

	case RespInt:
		return strconv.Atoi(val)

	case RespArray:
		size, err := strconv.Atoi(val)
		if err != nil {
			return nil, fmt.Errorf("array size error: %v", err)
		}
		return ScanRespArray(scanner, size)

	case RespBulk:
		size, err := strconv.Atoi(val)
		if err != nil {
			return nil, fmt.Errorf("bulk size error: %v", err)
		}
		return ScanRespBulk(scanner, size)
	}

	return kind + val, nil
}

func ScanRespArray(scanner *bufio.Scanner, size int) (interface{}, error) {

	if size < 0 {
		return nil, nil
	}

	vals := []interface{}{}

	for n := 0; n < size; n++ {
		val, err := ScanResp(scanner)
		if err != nil {
			return nil, err
		}
		vals = append(vals, val)
	}

	return vals, nil
}

func ScanRespBulk(scanner *bufio.Scanner, size int) (interface{}, error) {

	if size < 0 {
		return nil, nil
	}

	bulk := ""

	for scanner.Scan() {
		bulk += scanner.Text()
		if len(bulk) < size {
			bulk += "\n"
		} else {
			break
		}
	}

	return bulk[:size], nil
}
