package main

import (
	"context"
	"fmt"
	"sync"
)

type Store interface {
	Get(ctx context.Context, key string) (string, error)
	Set(ctx context.Context, key string, val string) error
	Delete(ctx context.Context, key string) error
}

var StoreKeyNotExistsErr = fmt.Errorf("key does not exist")

type MemoryStore struct {
	kv  map[string]string
	kvx *sync.RWMutex
}

func NewMemoryStore() *MemoryStore {
	return &MemoryStore{
		map[string]string{},
		&sync.RWMutex{},
	}
}

func (s *MemoryStore) Get(ctx context.Context, key string) (string, error) {

	s.kvx.RLock()
	defer s.kvx.RUnlock()

	if v, ok := s.kv[key]; ok {
		return v, nil
	}

	return "", StoreKeyNotExistsErr
}

func (s *MemoryStore) Set(ctx context.Context, key string, val string) error {

	s.kvx.Lock()
	defer s.kvx.Unlock()

	s.kv[key] = val

	return nil
}

func (s *MemoryStore) Delete(ctx context.Context, key string) error {

	s.kvx.Lock()
	defer s.kvx.Unlock()

	if _, ok := s.kv[key]; ok {
		delete(s.kv, key)
	} else {
		return StoreKeyNotExistsErr
	}

	return nil
}
